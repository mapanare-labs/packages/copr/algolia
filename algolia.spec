%global debug_package %{nil}

Name:    algolia
Version:        1.6.11
Release: 1%{?dist}
Summary: "Algolia's official command line tool"
Group:   Applications/System
License: MIT
Url:     https://github.com/%{name}/cli
Source0: %{url}/releases/download/v%{version}/%{name}_%{version}_linux_amd64.tar.gz

%description
A command line interface to enable Algolia developers to interact 
with and configure their Algolia applications straight from a command 
line or terminal window. Automate common workloads, create snapshots, 
revert to backups, or quickly modify applications as needed! 

This is a lightweight tool, providing a text-only interface, that is 
easy to install and use!

%prep
%setup -qn %{name}_%{version}_linux_amd64

%install
install -d -m 755 %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}

# Completions
install -Dpm 644 completions/algolia.bash %{buildroot}%{_datadir}/bash-completion/completions/%{name}
install -Dpm 644 completions/algolia.zsh %{buildroot}%{_datadir}/zsh/site-functions/_%{name}
install -Dpm 644 completions/algolia.fish %{buildroot}%{_datadir}/fish/vendor_completions.d/%{name}.fish

%files
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/zsh/site-functions/_%{name}
%{_datadir}/fish/vendor_completions.d/%{name}.fish

%changelog
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.5.0

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.4.2

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.3.7

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.3.5

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.2.1

* Wed Sep 28 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
